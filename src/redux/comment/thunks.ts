import { CommentData } from '@jmetterrothan/instalike';

import instalikeApi from '../../instalikeApi';
import { AppThunkAction } from '../types';
import { SendComment } from './actions';

export const sendCommentAsync = (postId: number, comment: CommentData): AppThunkAction<Promise<void>> => {
  return async (dispatch) => {
    try {
      const response = await instalikeApi.posts.find(postId).comments.create(comment);
      console.log(response);
      //   dispatch(SendComment(response.data));
    } catch (e) {
      console.log(e);
    }
  };
};
