import type { RootState } from '../store';

export const commentSelector = (state: RootState) => state.comment;
