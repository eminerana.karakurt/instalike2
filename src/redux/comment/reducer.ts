import { Reducer } from 'react';

import { Post } from '../../types/types';
import { CommentAction, SEND_COMMENT, UNSEND_COMMENT } from './actions';

type CommentState = {
  isLoad: boolean;
  data: [] | Post;
};

const initialState: CommentState = {
  isLoad: false,
  data: [],
};

const commentReducer: Reducer<CommentState, CommentAction> = (state = initialState, action) => {
  switch (action.type) {
    case SEND_COMMENT:
      console.log('reducer', state, action.payload);
      return {
        ...state,
        isLoad: true,
        data: action.payload,
      };
    case UNSEND_COMMENT:
      console.log(state);
      return {
        ...state,
        isLoad: false,
      };
    default:
      return state;
  }
};
export default commentReducer;
