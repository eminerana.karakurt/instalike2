import { Post } from '../../types/types';
import type { AppAction } from '../types';

export const SEND_COMMENT = 'COMMENTS/SEND';
export const UNSEND_COMMENT = 'COMMENTS/UNSEND';

export type SendComment = AppAction<typeof SEND_COMMENT, Post>;
export type UnsendComment = AppAction<typeof UNSEND_COMMENT>;
export type CommentAction = SendComment | UnsendComment;
export const sendComment = (Post: Post): SendComment => ({
  type: SEND_COMMENT,
  payload: Post,
});
export const unsendComment = (): UnsendComment => ({
  type: UNSEND_COMMENT,
  payload: null,
});
