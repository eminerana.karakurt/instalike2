import type { RootState } from '../store';

export const followSuggestions = (state: RootState) => state.follow['suggestions'];

export const followings = (state: RootState) => state.follow['following'];
