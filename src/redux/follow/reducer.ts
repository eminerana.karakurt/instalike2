import { Reducer } from 'react';

import { FollowAction, FOLLOW, UNFOLLOW, SUGGESTIONS, LIST_OF_FOLLOWING } from './actions';

type FollowState = {
  followers: [] | unknown;
  following: [] | unknown;
  suggestions: [] | unknown;
};

const initialState: FollowState = {
  followers: [],
  following: [],
  suggestions: [],
};

const followReducer: Reducer<FollowState, FollowAction> = (state = initialState, action) => {
  switch (action.type) {
    case FOLLOW:
      return {
        ...state,
        followers: action.payload,
      };
    case LIST_OF_FOLLOWING:
      return {
        ...state,
        following: action.payload,
      };
    case SUGGESTIONS:
      return {
        ...state,
        suggestions: action.payload,
      };
    default:
      return state;
  }
};

export default followReducer;
