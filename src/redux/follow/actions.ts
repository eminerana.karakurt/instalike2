import { UserData } from '../../types/types';
import type { AppAction } from '../types';

export const FOLLOW = 'FOLLOW/SEND';
export const UNFOLLOW = 'FOLLOW/UNSEND';
export const SUGGESTIONS = 'FOLLOW/SUGGESTIONS';
export const LIST_OF_FOLLOWING = 'FOLLOW/LIST_OF_FOLLOWING';
export type Follow = AppAction<typeof FOLLOW>;
export type UnFollow = AppAction<typeof UNFOLLOW>;
export type Suggestions = AppAction<typeof SUGGESTIONS>;
export type ListOfFollowing = AppAction<typeof LIST_OF_FOLLOWING>;

export type FollowAction = Follow | UnFollow | Suggestions | ListOfFollowing;

export const Follow = (): Follow => ({
  type: FOLLOW,
  payload: null,
});
export const UnFollow = (): UnFollow => ({
  type: UNFOLLOW,
  payload: null,
});
export const Suggestions = (data: UserData[]): Suggestions => ({
  type: SUGGESTIONS,
  payload: data,
});
export const ListOfFollowing = (data: unknown): ListOfFollowing => ({
  type: LIST_OF_FOLLOWING,
  payload: data,
});
