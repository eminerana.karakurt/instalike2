import instalikeApi from '../../instalikeApi';
import { AppThunkAction } from '../types';
import { Follow, UnFollow, Suggestions, ListOfFollowing } from './actions';

export const FollowSuggestions = (): AppThunkAction<Promise<void>> => {
  return async (dispatch) => {
    try {
      const response = await instalikeApi.users.me.followSuggestions.fetch({});
      console.log(response.data);
      dispatch(Suggestions(response.data));
    } catch (e) {
      console.log(e);
    }
  };
};
export const listOfFollowings = (): AppThunkAction<Promise<void>> => {
  return async (dispatch) => {
    try {
      const response = await instalikeApi.users.me.following.fetch();
      console.log(response.data);
      dispatch(ListOfFollowing(response.data));
    } catch (e) {
      console.log(e);
    }
  };
};
export const FollowAsync = (id: number): AppThunkAction<Promise<void>> => {
  return async (dispatch) => {
    try {
      const response = await instalikeApi.users.me.followers.follow(id);
      console.log(response.data);
      dispatch(Follow);
    } catch (e) {
      console.log(e);
    }
  };
};
export const UnFollowAsync = (id: number): AppThunkAction<Promise<void>> => {
  return async (dispatch) => {
    try {
      const response = await instalikeApi.users.me.followers.unfollow(id);
      console.log(response.data);
      dispatch(UnFollow);
    } catch (e) {
      console.log(e);
    }
  };
};
