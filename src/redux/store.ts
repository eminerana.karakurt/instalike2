import { applyMiddleware, combineReducers, createStore, Middleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import instalikeApi from '../instalikeApi';
import authReducer from './auth/reducer';
import commentReducer from './comment/reducer';
import discoverReducer from './discover/reducer';
import feedReducer from './feed/reducer';
import followReducer from './follow/reducer';
import postReducer from './post/reducer';
import postCreateReducer from './postCreate/reducer';

const rootReducer = combineReducers({
  auth: authReducer,
  feed: feedReducer,
  post: postReducer,
  discover: discoverReducer,
  postCreate: postCreateReducer,
  comment: commentReducer,
  follow: followReducer,
});

const middleware: Middleware[] = [];

middleware.push(thunk.withExtraArgument(instalikeApi));

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(...middleware)));

export type RootState = ReturnType<typeof rootReducer>;

export default store;
