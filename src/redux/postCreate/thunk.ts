import { PostData } from '@jmetterrothan/instalike';

import instalikeApi from '../../instalikeApi';
import { Post } from '../../types/types';
import { AppThunkAction } from '../types';
import { sendForm } from './actions';

export const createFormAsync = (post: PostData): AppThunkAction<Promise<void>> => {
  return async (dispatch, getState, api) => {
    try {
      console.log(typeof post.resources, post.resources);
      const response = await instalikeApi.posts.create(post);
      console.log('verif', response);
      // dispatch(sendForm());
    } catch (e) {
      console.log(e);
    }
  };
};
