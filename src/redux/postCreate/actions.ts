import { Post } from '../../types/types';
import { AppAction } from '../types';

export const SEND_FORM = 'FORM/SEND';

export type SendForm = AppAction<typeof SEND_FORM, Post>;

export const sendForm = (Post: Post): SendForm => ({
  type: SEND_FORM,
  payload: Post,
});
