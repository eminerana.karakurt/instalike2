import { Reducer } from 'react';

import { Post } from '../../types/types';
import { SendForm, SEND_FORM } from './actions';

type formState = {
  isFull: boolean;
  data: [] | unknown;
};

const initialState: formState = {
  isFull: false,
  data: [],
};

const postCreateReducer: Reducer<formState, SendForm> = (state = initialState, action) => {
  switch (action.type) {
    case SEND_FORM:
      return {
        ...state,
        isFull: true,
        data: action.payload,
      };
    default:
      return state;
  }
};
export default postCreateReducer;
