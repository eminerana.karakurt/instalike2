import { createSelector } from 'reselect';

import type { RootState } from '../store';

export const formSelector = (state: RootState) => state.postCreate;
