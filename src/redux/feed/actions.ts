/* actions : LOAD (charger données), UNLOAD pour post aussi, (LIKE, UNLIKE,) à mettre dans post, FOLLOW dans post aussi*/
import type { AppAction } from '../types';

export const LOAD_FEED = 'FEED/LOAD';
export const UNLOAD_FEED = 'FEED/UNLOAD';

export type LoadFeedAction = AppAction<typeof LOAD_FEED>;
export type UnloadFeedAction = AppAction<typeof UNLOAD_FEED>;

export type FeedAction = LoadFeedAction | UnloadFeedAction;

export const loadAction = (feed: unknown): LoadFeedAction => ({
  type: LOAD_FEED,
  payload: feed,
});

export const unloadAction = (): UnloadFeedAction => ({
  type: UNLOAD_FEED,
  payload: null,
});
