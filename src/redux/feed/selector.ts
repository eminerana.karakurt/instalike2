import type { RootState } from '../store';

export const feedSelector = (state: RootState) => state.feed;
