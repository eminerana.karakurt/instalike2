import instalikeApi from '../../instalikeApi';
import { AppThunkAction } from '../types';
import { loadAction, unloadAction } from './actions';

export const loadActionAsync = (i: number): AppThunkAction<Promise<void>> => {
  return async (dispatch) => {
    try {
      const response = await instalikeApi.users.me.feed.fetch({ amount: 5 * i, cursor: null });

      console.log(response.data.items);
      dispatch(loadAction(response.data.items));
    } catch (e) {
      console.log(e);
    }
  };
};
