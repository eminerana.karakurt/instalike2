import { Reducer } from 'redux';

import { FeedAction, LOAD_FEED, UNLOAD_FEED } from './actions';

type FeedState = {
  isLoad: boolean;
  data: [] | unknown;
};

const initialState: FeedState = {
  isLoad: false,
  data: [],
};

const feedReducer: Reducer<FeedState, FeedAction> = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_FEED:
      return {
        ...state,
        isLoad: true,
        data: action.payload,
      };
    case UNLOAD_FEED:
      return {
        ...state,
        isLoad: false,
      };
    default:
      return state;
  }
};

export default feedReducer;
