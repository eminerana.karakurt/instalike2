import type { RootState } from '../store';

export const discoverSelector = (state: RootState) => state.discover;
