import instalikeApi from '../../instalikeApi';
import { AppThunkAction } from '../types';
import { loadAction, unloadAction } from './actions';

// const params = {
//   amount: 6,
//   cursor: 'eyJjcmVhdGVkX2F0IjoiMjAyMy0wMi0yMiAyMTo1Nzo1NCIsIl9wb2ludHNUb05leHRJdGVtcyI6dHJ1ZX0',
// };
export const loadActionAsync = (): AppThunkAction<Promise<void>> => {
  return async (dispatch) => {
    try {
      let postData;
      let response = await instalikeApi.posts.fetch({ amount: 5, cursor: null });
      while (response.data.hasMorePages) {
        postData = response.data.items;
        response = await instalikeApi.posts.fetch({
          amount: postData.length + 5,
          cursor: response.data.cursor,
        });
      }
      postData = response.data.items;
      console.log(postData);
      dispatch(loadAction(postData));
    } catch (e) {
      console.log(e);
    }
  };
};
