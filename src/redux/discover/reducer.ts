import { Reducer } from 'redux';

import { DiscoverAction, LOAD_DISCOVER, UNLOAD_DISCOVER } from './actions';

type DiscoverState = {
  isLoad: boolean;
  data: [] | unknown;
};

const initialState: DiscoverState = {
  isLoad: false,
  data: [],
};

const discoverReducer: Reducer<DiscoverState, DiscoverAction> = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_DISCOVER:
      console.log(state, action.payload);
      return {
        ...state,
        isLoad: true,
        data: action.payload,
      };
    case UNLOAD_DISCOVER:
      console.log(state);
      return {
        ...state,
        isLoad: false,
      };
    default:
      return state;
  }
};

export default discoverReducer;
