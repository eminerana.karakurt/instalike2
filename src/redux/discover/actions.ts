import type { AppAction } from '../types';

export const LOAD_DISCOVER = 'DISCOVER/LOAD';
export const UNLOAD_DISCOVER = 'DISCOVER/UNLOAD';

export type LoadDiscoverAction = AppAction<typeof LOAD_DISCOVER>;
export type UnloadDiscoverAction = AppAction<typeof UNLOAD_DISCOVER>;

export type DiscoverAction = LoadDiscoverAction | UnloadDiscoverAction;

export const loadAction = (discover: any): LoadDiscoverAction => ({
  type: LOAD_DISCOVER,
  payload: discover,
});

export const unloadAction = (): UnloadDiscoverAction => ({
  type: UNLOAD_DISCOVER,
  payload: null,
});
