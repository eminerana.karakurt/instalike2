import instalikeApi from '../../instalikeApi';
import { Post } from '../../types/types';
import { AppThunkAction } from '../types';
import { loadAction, unloadAction, userLikePost, userUnlikePost } from './actions';

export const loadActionAsync2 = (id: number): AppThunkAction<Promise<void>> => {
  return async (dispatch, getState, api) => {
    try {
      const response = await instalikeApi.posts.find(id).fetch();
      console.log('verif', response);
      dispatch(loadAction(response.data as unknown as Post));
    } catch (e) {
      console.log(e);
    }
  };
};
export const userLikePostAsync = (Post: Post): AppThunkAction<Promise<void>> => {
  return async (dispatch, getState, api) => {
    try {
      const response = await instalikeApi.posts.find(Post.id).like();
      console.log(response, Post);
      dispatch(userLikePost(Post));
    } catch (e) {
      console.log(e);
    }
  };
};
export const userUnlikePostAsync = (Post: Post): AppThunkAction<Promise<void>> => {
  return async (dispatch, getState, api) => {
    try {
      const response = await instalikeApi.posts.find(Post.id).unlike();
      console.log(response, Post);
      dispatch(userUnlikePost(Post));
    } catch (e) {
      console.log(e);
    }
  };
};
