import { Post } from '../../types/types';
import type { AppAction } from '../types';

export const USER_LIKE_POST = 'POST/LIKE';
export const USER_UNLIKE_POST = 'POST/UNLIKE';
export const LOAD_POST = 'POST/LOAD';
export const UNLOAD_POST = 'POST/UNLOAD';

export type LoadPostAction = AppAction<typeof LOAD_POST, Post>;
export type UnloadPostAction = AppAction<typeof UNLOAD_POST>;
export type userLikePost = AppAction<typeof USER_LIKE_POST, Post>;
export type userUnlikePost = AppAction<typeof USER_UNLIKE_POST, Post>;

export type PostAction = LoadPostAction | UnloadPostAction | userLikePost | userUnlikePost;

export const loadAction = (Post: Post): LoadPostAction => ({
  type: LOAD_POST,
  payload: Post,
});

export const unloadAction = (): UnloadPostAction => ({
  type: UNLOAD_POST,
  payload: null,
});

export const userLikePost = (Post: Post): userLikePost => ({
  type: USER_LIKE_POST,
  payload: Post,
});

export const userUnlikePost = (Post: Post): userUnlikePost => ({
  type: USER_UNLIKE_POST,
  payload: Post,
});
