import { Reducer } from 'redux';

import { Post } from '../../types/types';
import { PostAction, LOAD_POST, UNLOAD_POST, USER_LIKE_POST, USER_UNLIKE_POST } from './actions';

type PostState = {
  id: number;
  isLoad: boolean;
  liked: boolean;
  data: Post;
};

const initialState: PostState = {
  isLoad: false,
  liked: false,
  data: {} as Post,
  id: 0,
};

const postReducer: Reducer<PostState, PostAction> = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_POST:
      console.log('reducer', state, action.payload);
      return {
        ...state,
        isLoad: true,
        data: action.payload,
      };
    case UNLOAD_POST:
      console.log(state);
      return {
        ...state,
        isLoad: false,
      };
    case USER_LIKE_POST:
      console.log('verif : ', state, action.payload);
      state.liked = true;
      action.payload.viewerHasLiked = state.liked;
      action.payload.likesCount++;
      return {
        ...state,
      };
    case USER_UNLIKE_POST:
      console.log('verif : ', state, action.payload);
      state.liked = false;
      action.payload.viewerHasLiked = state.liked;
      action.payload.likesCount--;
      return {
        ...state,
      };
    default:
      return state;
  }
};

export default postReducer;
