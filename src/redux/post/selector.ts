import type { RootState } from '../store';

export const postSelector = (state: RootState) => state.post;
