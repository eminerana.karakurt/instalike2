import * as i18n from 'i18next';
import { initReactI18next } from 'react-i18next';

import Language from './assets/enums/Language';
import CommonEn from './assets/locales/en/common.json';
import CommonFr from './assets/locales/fr/common.json';

i18n.use(initReactI18next).init({
  ns: 'common',
  defaultNS: 'common',
  fallbackLng: Language.EN,
  supportedLngs: Object.values(Language),
  resources: {
    [Language.EN]: {
      common: CommonEn,
    },
    [Language.FR]: {
      common: CommonFr,
    },
  },
  interpolation: {
    escapeValue: false,
  },
  detection: {
    order: ['queryString', 'localStorage'],
    caches: ['localStorage'],
  },
});
