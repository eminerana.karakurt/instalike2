function PostDesc(props: { desc: string | undefined }) {
  return <p className="text-start pl-4 pr-2">{props.desc}</p>;
}

export default PostDesc;
