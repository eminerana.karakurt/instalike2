function ProfileCard(props: { profileLetter: string | undefined; userID: number }) {
  const colors = [
    'bg-red-700',
    'bg-orange-500',
    'bg-amber-500',
    'bg-amber-900',
    'bg-yellow-700',
    'bg-lime-600',
    'bg-green-700',
    'bg-emerald-700',
    'bg-teal-700',
    'bg-cyan-700',
    'bg-sky-700',
    'bg-blue-700',
    'bg-violet-700',
    'bg-purple-700',
    'bg-fuchsia-700',
    'bg-pink-700',
    'bg-rose-700',
  ];

  return (
    <div
      className={
        'profileCard w-[50px] h-[50px] rounded-full flex items-center justify-center ' +
        colors[props.userID > colors.length ? props.userID % colors.length : props.userID]
      }
    >
      {/* <img src="https://picsum.photos/100/100" alt="" className="bg-cover w-full h-full rounded-full" /> */}
      <span className="text-white">{props.profileLetter}</span>
    </div>
  );
}

export default ProfileCard;
