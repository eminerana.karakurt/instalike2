import { faHeart, faComment } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { useEffect } from 'react';

function PictureCard(props: { src: string; likesCount: number; commentsCount: number; isFeed: boolean }) {
  useEffect(() => {
    console.log(props);
  }, []);
  return (
    <div className="pictureCard relative">
      <img
        src={props.src}
        alt=""
        className={props.isFeed ? 'bg-cover rounded w-full' : 'rounded min-h-full min-w-full w-auto h-auto'}
      />
      <div
        className={
          props.isFeed === false
            ? 'overlay opacity-0 hover:opacity-100 bg-slate-700/50 text-white transition-all flex absolute top-0 left-0 w-full h-full items-center justify-center gap-2'
            : 'hidden'
        }
      >
        <span>
          <FontAwesomeIcon icon={faHeart} />
          <span className="ml-2">{props.likesCount}</span>
        </span>
        <span>
          <FontAwesomeIcon icon={faComment} />
          <span className="ml-2">{props.commentsCount}</span>
        </span>
      </div>
    </div>
  );
}

export default PictureCard;
