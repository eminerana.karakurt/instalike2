import PostHeaderMenu from '../menus/PostHeaderMenu';
import PostReactionsMenu from '../menus/PostReactionsMenu';
import PictureCard from './PictureCard';
import PostDesc from './PostDesc';

function PostCard(props: {
  userID: number;
  Id: number;
  children?: JSX.Element | JSX.Element[];
  src: string;
  desc: string;
  location: string;
  userName: string;
  likesCount: number;
  commentsCount: number;
  liked: boolean;
  isFollowed: boolean;
  isFeed: boolean;
  onClickHandle: () => unknown;
}) {
  return (
    <div className={'postCard w-[550px] space-y-4 border pb-4 my-4 bg-white ' + props.Id}>
      <PostHeaderMenu
        location={props.location}
        userName={props.userName}
        userID={props.userID}
        Id={props.Id}
        isFollowed={props.isFollowed}
      ></PostHeaderMenu>
      <PictureCard src={props.src} likesCount={0} commentsCount={0} isFeed={props.isFeed}></PictureCard>
      <PostDesc desc={props.desc}></PostDesc>
      <PostReactionsMenu
        id={props.Id}
        link={true}
        likesCount={props.likesCount}
        commentsCount={props.commentsCount}
        liked={props.liked}
        onClickHandle={props.onClickHandle}
      ></PostReactionsMenu>
      {props.children}
    </div>
  );
}

export default PostCard;
