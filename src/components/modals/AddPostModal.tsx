import { useEffect, useState } from 'react';

import useAppDispatch from '../../hooks/useAppDispatch';
import { createFormAsync } from '../../redux/postCreate/thunk';

/* eslint-disable jsx-a11y/label-has-associated-control */
const AddPostModal = () => {
  const dispatch = useAppDispatch();
  const [file, setFile] = useState<File[]>([]);
  const [caption, setCaption] = useState('');
  const [localisation, setLocalisation] = useState('');
  const [commentsDis, setCommentsDis] = useState(false);
  const post = {
    resources: file,
    location: localisation,
    caption: caption,
    accessibilityCaption: '',
    hasCommentsDisabled: commentsDis,
  };
  useEffect(() => {
    console.log('file', file);
    console.log('caption', caption);
    console.log('localisation', localisation);
    console.log('comments', commentsDis);
  }, [dispatch]);
  const onClickhandle = () => {
    return dispatch(createFormAsync(post));
  };
  return (
    <div>
      <input type="checkbox" id="my-modal-4" className="modal-toggle" />
      <label htmlFor="my-modal-4" className="modal cursor-pointer">
        <label className="modal-box relative w-[50vw] h-3/4 max-w-full" htmlFor="">
          <h2 className="text-3xl">Create a new post</h2>
          <div className="flex justify-between">
            <input
              type="file"
              name=""
              id=""
              onChange={(e) => {
                setFile([...(e.target.files as FileList)]);
              }}
            />
            <div>
              <textarea
                placeholder="Write a caption"
                className="textarea textarea-bordered"
                onChange={(e) => setCaption(e.target.value)}
              ></textarea>
              <input
                type="text"
                placeholder="Localisation"
                className="input input-bordered w-full max-w-xs"
                onChange={(e) => setLocalisation(e.target.value)}
              />
              <label htmlFor="toggle">Disable comments</label>
              <input
                id="toggle"
                type="checkbox"
                className="toggle toggle-info"
                onChange={(e) => setCommentsDis(e.target.checked)}
              />
            </div>
          </div>
          <div>
            <label htmlFor="my-modal-4" className="btn btn-ghost modal-action">
              Cancel
            </label>
            <button className="btn btn-primary" onClick={onClickhandle}>
              Submit
            </button>
          </div>
        </label>
      </label>
    </div>
  );
};

export default AddPostModal;

function onSubmitHandle(arg0: (e: any) => void) {
  throw new Error('Function not implemented.');
}
