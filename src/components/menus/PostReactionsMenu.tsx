import { faHeart, faCommentDots } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';

function PostReactionsMenu(props: {
  id: number | undefined;
  link: boolean | undefined;
  likesCount: number | undefined;
  commentsCount: number | undefined;
  liked: boolean;
  onClickHandle: () => any;
}) {
  return (
    <>
      <div className="postReactionsMenu mb-8 space-x-4 text-start pl-4">
        <button
          onClick={props.onClickHandle}
          className={
            props.liked === true
              ? ' bg-red-400 rounded-full px-4 py-2 text-white'
              : ' bg-slate-300 rounded-full px-4 py-2'
          }
        >
          {' '}
          <FontAwesomeIcon icon={faHeart} className="mr-1" /> {props.likesCount}
        </button>
        <span>
          {props.link ? (
            <Link to={'/post/' + props.id}>
              <FontAwesomeIcon icon={faCommentDots} className="mr-1" />
              {props.commentsCount}
            </Link>
          ) : (
            <>
              <FontAwesomeIcon icon={faCommentDots} className="mr-1" />
              {props.commentsCount}
            </>
          )}
        </span>
      </div>
    </>
  );
}

export default PostReactionsMenu;
