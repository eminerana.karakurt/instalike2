import { faEllipsisVertical } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

import useAppDispatch from '../../hooks/useAppDispatch';
import { FollowAsync, UnFollowAsync } from '../../redux/follow/thunk';
import ProfileCard from '../cards/ProfileCard';

function PostHeaderMenu(props: {
  userName: string | undefined;
  location: string | undefined;
  userID: number;
  Id: number;
  isFollowed: boolean;
}) {
  const [showMenu, setShowMenu] = useState(false);
  const [isFollowed, setIsFollowed] = useState(props.isFollowed); // add state variable for follow status
  const dispatch = useAppDispatch();

  const handleCopyLink = (e: string) => {
    navigator.clipboard.writeText(e);
  };

  const handleFollow = () => {
    if (isFollowed) {
      dispatch(UnFollowAsync(props.userID));
      setIsFollowed(false); // update follow status to unfollowed
    } else {
      dispatch(FollowAsync(props.userID));
      setIsFollowed(true); // update follow status to followed
    }
  };

  return (
    <div className="flex postHeaderMenu justify-between p-4 items-center">
      <div className="flex space-x-4">
        <ProfileCard profileLetter={props.userName?.charAt(0).toUpperCase()} userID={props.userID}></ProfileCard>
        <div className="">
          <p className="postHeaderMenu-author w-fit">{props.userName}</p>
          <div className="text-slate-500">
            {props.location !== '' ? (
              <>
                <span className="postHeaderMenu-localisation">{props.location}</span> <span>,</span>{' '}
              </>
            ) : null}
            <span className="postHeaderMenu-hour">3 minutes ago</span>
          </div>
        </div>
        {isFollowed === false ? (
          <button className="bg-slate-200 text-stone-800 px-1 rounded" onClick={handleFollow}>
            Follow
          </button>
        ) : null}
      </div>
      <div>
        <button onClick={() => setShowMenu(!showMenu)} className="p-2">
          <FontAwesomeIcon icon={faEllipsisVertical} />
        </button>
        <ul className={'bg-white drop-shadow-sm rounded absolute p-3 z-10' + ' ' + (showMenu ? 'block' : 'hidden')}>
          <li>
            <button onClick={handleFollow}>{isFollowed ? 'Unfollow' : 'Follow'}</button>
          </li>
          <li>
            <Link to={'/post/' + props.Id}>See publication</Link>
          </li>
          <li>
            <button onClick={() => handleCopyLink(window.location.origin + '/post/' + props.Id)}>Copy link</button>
          </li>
        </ul>
      </div>
    </div>
  );
}

export default PostHeaderMenu;
