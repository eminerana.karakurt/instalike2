import { faSmile } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import EmojiPicker from 'emoji-picker-react';
import { useState } from 'react';

import useAppDispatch from '../../hooks/useAppDispatch';
import { sendCommentAsync } from '../../redux/comment/thunks';
import ProfileCard from '../cards/ProfileCard';

function CommentForm(props: { postID: number; userID: number }) {
  const dispatch = useAppDispatch();
  const [message, setMessage] = useState('');
  const onClickhandle = () => {
    return dispatch(sendCommentAsync(props.postID, { text: message, mentions: [] }));
  };

  return (
    <div className="commentForm flex flex-col w-full border border-slate-500 bg-white">
      <div className="flex w-full py-4 border-b border-slate-400">
        <ProfileCard profileLetter={''} userID={props.userID}></ProfileCard>
        <form className="flex-1">
          <input
            type="text"
            name="comment"
            id="comment"
            className="w-full h-[70px]"
            onChange={(e) => {
              setMessage(e.target.value);
            }}
          />
        </form>
      </div>
      <div className="self-end space-x-3 py-2 px-3">
        <button type="button" className="bg-slate-400 w-fit px-2 rounded-full ">
          <FontAwesomeIcon icon={faSmile}></FontAwesomeIcon>
        </button>
        <button type="button" className="bg-slate-400 w-fit px-2 rounded-full" onClick={onClickhandle}>
          Publish
        </button>
      </div>
    </div>
  );
}
export default CommentForm;
