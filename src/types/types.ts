export type Post = {
  resources: [{ src: string }];
  accessibilityCaption: string;
  caption: string;
  commentsCount: number;
  createdAt: string;
  hasCommentsDisabled: boolean;
  id: number;
  likesCount: number;
  location: string;
  owner: Owner;
  previewComments: [];
  previewLikes: [];
  ressourceType: string;
  ressources: [];
  updatedAt: string;
  viewerHasLiked: boolean;
};

export type Owner = {
  biography: string;
  createdAt: string;
  email: string;
  firstName: string;
  followersCount: number;
  followingCount: number;
  fullName: string;
  id: number;
  isFollowedByViewer: boolean;
  isViewer: boolean;
  lastName: string;
  postsCount: boolean;
  resourceType: string;
  updatedAt: string;
  userName: string;
};

export type UserData = {
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  biography: string;
};
