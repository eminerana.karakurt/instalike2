// import { LanguageDetector } from 'react-i18next';
import { Navigate, Routes, Route } from 'react-router-dom';

import './App.css';
import AuthGuard from './components/AuthGuard';
import useAppDispatch from './hooks/useAppDispatch';
import './i18n';
import DiscoverView from './views/DiscoverView';
import FeedView from './views/FeedView';
import LoginView from './views/LoginView';
import PostView from './views/PostView';

// import instalikeApi from './instalikeApi';

function App() {
  const dispatch = useAppDispatch();
  return (
    <Routes>
      <Route path="login" element={<LoginView />}></Route>
      <Route element={<AuthGuard />}>
        <Route path="feed" index element={<FeedView />}></Route>
        <Route path="post/:id" element={<PostView />}></Route>
        <Route path="discover" element={<DiscoverView />}></Route>
      </Route>

      <Route path="*" element={<Navigate to="feed" />}></Route>
    </Routes>
  );
}

export default App;
