import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Navigate, useParams } from 'react-router-dom';

import CommentList from '../components/CommentList';
import PostCard from '../components/cards/PostCard';
import CommentForm from '../components/forms/CommentForm';
import PostMenu from '../components/menus/PostMenu';
import useAppDispatch from '../hooks/useAppDispatch';
import { postSelector } from '../redux/post/selector';
import { loadActionAsync2 } from '../redux/post/thunk';

const usePostId = () => {
  const { id } = useParams();
  return id ? parseInt(id, 10) : -1;
};

const PostView = () => {
  const id = usePostId();

  const dispatch = useAppDispatch();
  const post = useSelector(postSelector);

  useEffect(() => {
    dispatch(loadActionAsync2(id));
  }, [dispatch, id]);

  useEffect(() => {
    if (post) {
      console.log('view : ', post);
    }
  }, [post]);
  if (id === -1) {
    return <Navigate to=".." />;
  }

  return (
    <>
      <PostMenu classCustom={' self-stretch mb-8'}></PostMenu>
      <div className="postView flex items-center justify-center bg-slate-800 w-screen absolute top-0 left-0 bg-opacity-60">
        {post.isLoad ? (
          <div className="wrapper">
            <PostCard
              Id={id}
              userID={post.data.owner.id}
              src={post.data.resources[0].src}
              desc={post.data.caption}
              location={post.data.location}
              userName={post.data.owner.userName}
              likesCount={post.data.likesCount}
              commentsCount={post.data.commentsCount}
              liked={post.data.viewerHasLiked}
              isFollowed={post.data.owner.isFollowedByViewer}
              isFeed={false}
              onClickHandle={() => {
                console.log('verif');
              }}
            >
              <CommentForm postID={id} userID={post.data.owner.id}></CommentForm>
              <CommentList></CommentList>
            </PostCard>
          </div>
        ) : (
          <div className="loader"></div>
        )}
      </div>
    </>
  );
};

export default PostView;
