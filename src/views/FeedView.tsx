import { faCheck, faEllipsisVertical, faPlus } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useEffect, ReactElement, useRef, useState } from 'react';
import { useSelector } from 'react-redux';

import PostCard from '../components/cards/PostCard';
import ProfileCard from '../components/cards/ProfileCard';
import PostMenu from '../components/menus/PostMenu';
import useAppDispatch from '../hooks/useAppDispatch';
import instalikeApi from '../instalikeApi';
import { loadActionAsync } from '../redux/feed/thunks';
import { followSuggestions, followings } from '../redux/follow/selector';
import { FollowAsync, FollowSuggestions } from '../redux/follow/thunk';
import { userLikePostAsync, userUnlikePostAsync } from '../redux/post/thunk';
import { Post } from '../types/types';

const FeedView = (): ReactElement => {
  const dispatch = useAppDispatch();
  const feed = useSelector((state: any) => state.feed);
  const followSuggestionsSelector = useSelector(followSuggestions);
  const followingsSelector = useSelector(followings);
  const [followCheck, setFollowCheck] = useState<{ id: number; follow: boolean }[]>([]);

  // const follow = useSelector(followers);
  const hrRef = useRef(null);
  const [page, setPage] = useState(1);
  // const posts = usePost();
  useEffect(() => {
    instalikeApi.posts.fetch({ cursor: null });
    dispatch(loadActionAsync(1));
    dispatch(FollowSuggestions());
  }, []);
  useEffect(() => {
    const initialFollowCheck = (followSuggestionsSelector as []).map((suggestion: { id: number }) => ({
      id: suggestion.id,
      follow: false,
    }));
    setFollowCheck(initialFollowCheck);
  }, [followSuggestionsSelector]);
  useEffect(() => {
    console.log('followCheck', followCheck);
  }, [followCheck]);
  useEffect(() => {
    console.log('feed', feed);
  }, [feed]);
  useEffect(() => {
    if (feed.isLoad) {
      const observer = new IntersectionObserver((entries) => {
        entries.forEach((entry) => {
          if (entry.isIntersecting) {
            setPage((prev) => prev + 1);
            dispatch(loadActionAsync(page));
          }
        });
      });
      if (hrRef.current) {
        observer.observe(hrRef.current);
      }

      // Nettoyage : arrêter l'observation lorsque le composant est démonté
      return () => {
        if (hrRef.current) {
          observer.unobserve(hrRef.current);
        }
      };
    }
  }, [feed, hrRef, dispatch, page]);

  return (
    <div id="wrapper" className="flex flex-col items-center">
      <PostMenu classCustom={' self-stretch mb-8'}></PostMenu>
      <div className="flex">
        {(followSuggestionsSelector as []).map((item: { id: number; userName: string }, i: number) => {
          return (
            <div className="flex flex-col" key={i}>
              <ProfileCard
                key={item.id}
                profileLetter={item.userName.charAt(0).toUpperCase()}
                userID={item.id}
              ></ProfileCard>
              <button
                className={'bg-blue-500 text-white rounded-full px-4 py-2'}
                onClick={() => {
                  dispatch(FollowAsync(item.id));
                  setFollowCheck((prev) =>
                    prev.map((check) => (check.id === item.id ? { ...check, follow: true } : check))
                  );
                }}
              >
                {followCheck[i]?.follow ? <FontAwesomeIcon icon={faCheck} /> : <FontAwesomeIcon icon={faPlus} />}
              </button>
            </div>
          );
        })}
      </div>
      {feed.data.map((item: Post, i: number) => {
        return (
          <PostCard
            userID={item.owner.id}
            Id={item.id}
            key={item.id}
            src={item.resources[0].src}
            desc={item.caption}
            userName={item.owner.userName}
            location={item.location}
            likesCount={item.likesCount}
            commentsCount={item.commentsCount}
            liked={item.viewerHasLiked}
            isFollowed={item.owner.isFollowedByViewer}
            isFeed={true}
            onClickHandle={() => {
              if (item.viewerHasLiked) {
                dispatch(userUnlikePostAsync(item));
              } else {
                dispatch(userLikePostAsync(item));
              }
            }}
          >
            <ul className="pl-3 space-y-4 text-slate-500">
              {item.previewComments.map(
                (comm: { id: number; owner: { userName: string; id: number }; text: string }) => {
                  console.log('comm', comm);
                  return (
                    <li key={comm.id} className="flex gap-2">
                      <ProfileCard profileLetter={comm.owner.userName.charAt(0).toUpperCase()} userID={comm.owner.id} />
                      <div className="flex flex-col items-start">
                        <span>{comm.owner.userName}</span>
                        {comm.text}
                      </div>
                    </li>
                  );
                }
              )}
            </ul>
          </PostCard>
        );
      })}

      <hr ref={hrRef} />
    </div>
  );
};

export default FeedView;
