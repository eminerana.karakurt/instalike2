import { useEffect } from 'react';
import { useSelector } from 'react-redux';

import PictureCard from '../components/cards/PictureCard';
import PostMenu from '../components/menus/PostMenu';
import useAppDispatch from '../hooks/useAppDispatch';
import { loadActionAsync } from '../redux/discover/thunks';

const DiscoverView = () => {
  const dispatch = useAppDispatch();

  const discover = useSelector((state: { discover: { isLoad: boolean; data: [] } }) => state.discover);
  useEffect(() => {
    dispatch(loadActionAsync());
    console.log(discover);
  }, [dispatch]);
  return (
    <div id="wrapper" className="flex flex-col items-center">
      <PostMenu classCustom={' self-stretch mb-8'}></PostMenu>
      <div className="grid grid-cols-3 gap-4 px-24">
        {discover.data.map(
          (
            item: {
              commentsCount: number;
              likesCount: number;
              resources: [{ src: string }];
            },
            i: number
          ) => {
            return (
              <PictureCard
                key={i}
                src={item.resources[0].src}
                likesCount={item.likesCount}
                commentsCount={item.commentsCount}
                isFeed={false}
              />
            );
          }
        )}
      </div>
    </div>
  );
};

export default DiscoverView;
