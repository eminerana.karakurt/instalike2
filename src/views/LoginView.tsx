import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import useAppDispatch from '../hooks/useAppDispatch';
import { loginAsync } from '../redux/auth/thunks';

function LoginView() {
  const dispatch = useAppDispatch();
  const [email, setEmail] = useState('emine-rana.karakurt@etu.unistra.fr');
  const [password, setPassword] = useState('DWEB2023');
  const [message, setMessage] = useState('');
  const navigate = useNavigate();
  return (
    <div className="login flex flex-col items-center justify-center h-screen w-screen">
      <div className="w-96 flex flex-col items-stretch ">
        <h1 className="logoType text-7xl self-start">Instalike</h1>
        <p className="mb-2 self-start">Welcome !</p>
        <p className="my-2 py-2 bg-red-200 border-red-500 text-red-500 rounded">{message}</p>
        <form className="login-form flex flex-col" data-cy="form">
          <input
            type="email"
            name="email"
            id="email"
            className="bg-slate-200 text-slate-600 rounded h-12 px-2 mb-5"
            placeholder="Email"
            onChange={(e) => {
              setEmail(e.target.value);
            }}
            data-cy="email"
          />
          <input
            type="password"
            name="password"
            id="password"
            className="bg-slate-200 text-slate-600 rounded h-12 px-2 mb-10"
            placeholder="Password"
            onChange={(e) => {
              setPassword(e.target.value);
            }}
            data-cy="password"
          />
          <button
            type="button"
            className="bg-sky-600 text-white h-12 font-semibold rounded"
            onClick={() => {
              dispatch(loginAsync(email, password))
                .then(() => {
                  navigate('/feed');
                })
                .catch((e) => setMessage(e.response.data.message));
            }}
            data-cy="submit"
          >
            Login
          </button>
        </form>
      </div>
    </div>
  );
}

export default LoginView;
