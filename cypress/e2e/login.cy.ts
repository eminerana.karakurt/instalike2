export default describe('Login', () => {
  it('passes', () => {
    cy.visit('/');
    cy.get('[data-cy="form"]').within(() => {
      cy.get('[data-cy="email"]').type('emine-rana.karakurt@etu.unistra.fr');
      cy.get('[data-cy="password"]').type('DWEB2023');
      // cy.get('[data-cy="submit"]').click();
      // cy.login('emine-rana.karakurt@etu.unistra.fr', 'DWEB2023');
      cy.root().submit();
    });
  });
});

// declare global {
//   namespace Cypress {
//     interface Chainable {
//       login(email: string, password: string): Chainable<void>;
//     }
//   }
// }
